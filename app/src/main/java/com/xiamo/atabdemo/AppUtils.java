package com.xiamo.atabdemo;

/**
 * @author ：
 * @date ：2020-10-19 16：09
 * 邮箱 ：chenxupei@tenzhao.com
 * 公司 ：浙江腾朝互联科技有限公司
 * 描述 ：
 */
public class AppUtils {
    // 两次点击按钮之间的点击间隔不能少于1000毫秒
    private static final int MIN_CLICK_DELAY_TIME = 500;
    private static long lastClickTime;

    public static boolean isInvalidClick() {
        long time = System.currentTimeMillis();
        if ((System.currentTimeMillis() - lastClickTime) < MIN_CLICK_DELAY_TIME) {
            return true;
        }else {
            lastClickTime = time;
        }
        return false;
    }

    private boolean intercept=false;

    public static boolean isFastClick() {
        boolean flag = false;
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME) {
            flag = true;
        }
        lastClickTime = curClickTime;
        return flag;
    }
}
